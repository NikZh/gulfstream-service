<?php
namespace Gulfstream\MagentoApi;

use SoapClient;

class M2 extends AbstractApi
{
    const PREFIX = 'ecomitizeGulfstreamSubscriptionV1';

    protected $url;

    protected $token;

    protected $connection;

    public function __construct($url, $token)
    {
        $this->url = $url;
        $this->token = $token;
    }

    public function getPrefix()
    {
        return self::PREFIX;
    }

    protected function getConnection()
    {
        if (!$this->connection) {
            $wsdlUrl = rtrim($this->url, '/') . '/soap/?services=' . $this->getPrefix() . '&wsdl';
            $options = ['http' => ['header' => "Authorization: Bearer " . $this->token]];
            $params = [
                'soap_version'   => SOAP_1_2,
                'stream_context' => stream_context_create($options),
//                'cache_wsdl'     => WSDL_CACHE_NONE
            ];
            $this->connection = new SoapClient($wsdlUrl, $params);
        }
        return $this->connection;
    }

    public function getSubscriptions()
    {
        $result = parent::getSubscriptions();
        $result = $result['items']['item'];
        if (isset($result['code'])) {
            return [$result];
        }
        return $result;
    }

    public function setSubscription($code, $url)
    {
        $result = parent::setSubscription($code, $url);
        return $result['id'];
    }

    public function getEvents()
    {
        $result = parent::getEvents();
        return $result['items']['item'];
    }

    protected function prepareResults($results)
    {
        $results = parent::prepareResults($results);
        return $results['result'];
    }
}
