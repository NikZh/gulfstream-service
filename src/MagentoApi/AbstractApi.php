<?php
namespace Gulfstream\MagentoApi;

abstract class AbstractApi
{
    /**
     * Base params used in soap call
     *
     * @var array
     */
    protected $baseApiParams = [];

    abstract public function getPrefix();

    /**
     * @return \SoapClient
     */
    abstract protected function getConnection();

    /**
     * Api methods
     */
    public function getSubscriptions()
    {
        return $this->getSoapResponse('getSubscriptions');
    }

    public function getSubscription($code)
    {
        $data = ['code' => $code];
        return $this->getSoapResponse('getSubscription', $data);
    }

    public function setSubscription($code, $url)
    {
        $data = ['subscription' => ['code' => $code, 'url' => $url]];
        return $this->getSoapResponse('setSubscription', $data);
    }

    public function removeSubscription($code)
    {
        $data = ['code' => $code];
        return $this->getSoapResponse('removeSubscription', $data);
    }

    public function getEvents()
    {
        return $this->getSoapResponse('getEvents');
    }

    /**
     * Send soap request and prepare response
     *
     * @param string $methodName
     * @param array $data
     * @return array
     */
    protected function getSoapResponse($methodName, $data = [])
    {
        $connection = $this->getConnection();
        $result = $connection->__soapCall($this->getPrefix() . $methodName, array_merge($this->baseApiParams, [$data]));
        return $this->prepareResults($result);
    }

    /**
     * Response results to array
     *
     * @param $results
     * @return array
     */
    protected function prepareResults($results)
    {
        return json_decode(json_encode($results), true);
    }

}
