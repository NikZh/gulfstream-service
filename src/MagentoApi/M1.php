<?php
namespace Gulfstream\MagentoApi;

use SoapClient;

class M1 extends AbstractApi
{
    const PREFIX = 'ecoGulfstream';

    /**
     * Magento base URL like http://msd.nikzh.pp.ua/
     *
     * @var string
     */
    protected $url;

    /**
     * Magento api username
     *
     * @var string
     */
    protected $username;

    /**
     * Magento api key
     *
     * @var string
     */
    protected $password;

    protected $useCache = WSDL_CACHE_NONE;

    protected $connection;

    /**
     * M1Api constructor.
     * @param string $url
     * @param string $username
     * @param string $password
     */
    public function __construct($url, $username, $password)
    {
        $this->url = $url;
        $this->username = $username;
        $this->password = $password;
    }

    public function getPrefix()
    {
        return self::PREFIX;
    }

    /**
     * Prepare soap connection
     *
     * @return SoapClient
     */
    protected function getConnection()
    {
        if (!$this->connection) {
            $wsdlUrl = rtrim($this->url, '/') . '/api/v2_soap/?wsdl=1';
            $params = [
//                'cache_wsdl'     => $this->useCache
            ];
            $this->connection = new SoapClient($wsdlUrl, $params);
            $this->baseApiParams[0] = $this->connection->login($this->username, $this->password);
        }
        return $this->connection;
    }
}
