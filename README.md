# README #

### Installation ###

* composer config repositories.gulfstream vcs git@bitbucket.org:NikZh/gulfstream-service.git
* composer require ecomitize/gulfstream-service

### Usage examples ###


```
#!php

$username = 'ecoApi';
$password = 'apikey';

//For Magento 1. $username - Api user, $password - User api key
$connection = new \Gulfstream\MagentoApi\M1('http://msd.nikzh.pp.ua/', $username, $password);

//For Magento 2. Second param - Magento api user Access Token (http://screencast.com/t/CkOTdBFXHA)
$connection = new \Gulfstream\MagentoApi\M2('http://ms2d.nikzh.pp.ua/', '9en4awl3mnmyeal9ue9aven85s7evvwe');

//Methods 

//get already subscribed events
$subscriptions = $connection->getSubscriptions();

//get single subscription info (if you already subscribed for this event)
$subscription = $connection->getSubscription('product.update');

//subscribe for event
$resultId = $connection->setSubscription('product.create', 'http://test-me.com/');

//remove event subscription
$boolValue = $connection->removeSubscription('product.update');

//get available events
$events = $connection->getEvents();
```